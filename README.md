# Tuto condensé mais complet pour passer à (Neo-)VIM

NeoVIM est une réécriture complète de VIM.

Petit rappel théorique, le principe de VIM c'est:
  - d'avoir un éditeur puissant (ie. des commandes qui font le café, comme dans emacs)
  - mais ergonomique: pas besoin d'avoir des mains immenses (comme dans emacs) ou d'appuyer frénétiquement sur la même touche (comme Echap avec emacs)
  - de gagner du temps en évitant les tâches répétitives, ainsi que l'usage de la souris (et donc les aller-retour souris-clavier)
  - de ne nécésiter qu'un terminal pour s'éxecuter
  - d'être assez intuitif une fois qu'on en a compris la logique (sans avoir besoin d'apprendre le LISP)

## Setup

$ apt install neovim

## Les commandes

Voici la centaine de commandes que j'utilise régulièrement, triées par thèmes, des plus essentielles aux bonus.

Inutile d'essayer de tout se rappeler d'un coup: commences par le indispensables, et exercez-vous à les utiliser 
sur la tâche qui vous occupe. Une fois à l'aise, vous pourrez rajouter le reste des commandes progressivement.

### Les indispensables:

Bon un tuto `vi` commence en général par `h j k l` pour déplacer le curseur, mais
perso, je n'ai jamais réussi à m'y faire: j'utilise les flèches à la place.

```text
i            insérer avant le caractère sous le curseur
yy Y         deux façon de copier la ligne courante (yank)
p            coller (paste)
u            undo  (donc, garder appuyer pour revenir dans l'état au début de la session...)
<C-R>        redo (C pour control)
/            chercher
n            continuer la recherche (next)
p            précédent résultat de recherche (previous)
<C-O>        revenir à la position précédente
v            passer en mode sélection (visual)
<S-V>        idem pour sélection des lignes entières (S pour shift)
<C-V>        sélection block
:s/a/b/      commande sed: remplacer la première occurence de a par b dans la ligne courante, combinable avec une sélection
:s/a/b/g     remplacer toutes les occurences de la ligne (option g)
:%s/a/b/gc   remplacer toutes les occurences du fichier (%) et ajout de l'option c pour avoir des confirmations
:term        ouvrir un terminal
:!ls         exécuter une commande (ici ls)
:make        invoquer le système de build (cf makeprg si pas de makefile)
:e           re-ouvrir le fichier depuis le disque (ajouter ! pour écraser les changements non sauvés) (edit)
:w           sauve le fichier (write)
:q           ferme la fenetre courante (ajouter ! pour perdre les changements non sauvés) (quit)
:wqa         sauver et quitter toutes les fenêtres
:help truc   demande de l'aide sur "truc"
```

### Les commandes en mode insertion:

```text
<Echap>     revenir en mode normal
<C-P>       complète le mots en utilisant le mot précédent qui commence pareil (appuyer plusieurs fois pour choisir) (previous)
<C-N>       idem avec le mot suivant (next)
```

### Naviguer entre les erreurs du compilateur:

```text
:cc        aller et afficher l'erreur courante (compiler error current)
:cp        erreur précédente (compiler error previous)
:cn        erreur suivante (compiler error next)
```

### Les macros:

```text
qq       démarrer la création de la macro q (on peut faire autant de macros qu'on veut: qa pour la macro a, etc.)
@q       appliquer une fois la macro q
```

### Se déplacer (très utile pour faire des macros):

```text
{ }         ligne vide précédente ou suivante
%           parenthèse/accolade/crocher correspondant
w b         mot suivant ou précedent (word, back)
W B         block sans espace suivant ou précédent
e E         fin du mot ou du block suivant (end)
0           comme origin, va au début de la ligne
$           comme end, va à la fin de la ligne
G           comme ctrl+end: va à la fin du fichier (goto)
23G ou :23  va à la ligne 23 du fichier
gg          comme ctrl+origin: va au début du fichier
( )         phrase suivante ou précédente
<C-B> <C-F> comme PgUp/PgDown (back, forward)
<C-U> <C-D> idem (up, down) mais violent (~1/2 page scrollée)
```

### Les petits plus pour gagner du temps, surtout dans les macros:

```text
*        rechercher le mot sous le curseur
g*       rechercher le mot sous le curseur même s'il est contenu dans un mot plus long
gu gU    passer en minuscule/majuscule
o O      insérer une nouvelle ligne après/avant
I        insérer au début de la ligne (ou de la sélection block)
d        combiné avec un déplacement, supprimer (delete)
dd       supprimer la ligne courante
x        supprimer le caractère sous le curseur (comme <Del>)
r        remplacer le caractère sous le curseur
c        remplacer la sélection ou le déplacement suivant (change)
R        passer en mode écrasement (rewrite)
C        remplacer la ligne courante
A        insérer à la fin de la ligne (append)
a        insérer après le caractère sous le curseur
.        refaire la dernière commande (très utile quand on ne veut pas prendre le temps de faire une macro)
fa       avancer au prochain caractère a dans la ligne (find)
Fb       reculer au précédent caractère b dans la ligne
;        refaire le dernier déplacement f ou F
"ay      utiliser le registre nommé a pour la prochaine commande (copier ou coller)
<C-A>    incrémenter le nombre sous le curseur ou l'identifiant terminé par un nombre
<C-X>    décrémenter
```

### Les fenêtres et les buffers :

```text
:sp            horizontal split (on peut ajouter un nom de fichier ou 
:sp <file>     horizontal split et ouverture d'un fichier
:vsp +term     vertical split avec ouverture d'un terminal 
<C-W>          en ajoutant un déplacement, navigue entre les fenetres
<C-W> <C-S>    horizontal split (un synonyme pour les gens qui aiment les raccourcis à la emacs)
<C-W> <C-V>    vertical split (idem)
<C-W> <C-X>    swap avec la dernière fenetre
:bd            ferme le buffer sans fermer la fenetre
:bn :bp        navigue entre buffers
:ls            afficher la liste des buffers ouverts
:b4            afficher le buffer numéro 4 (cf liste avec :ls)
:b#            affiche le précédent buffer utilisé par la fenêtre courante
<C-W> >        augmente la largeur de la fenêtre
<C-W> <        diminue la largeur de la fenêtre
<C-W> +        augmente la hauteur de la fenêtre 
<C-W> -        diminue la hauteur de la fenêtre 
```

### La gestion des fichiers:

```text
:w <cfile>   sauver dans le chemin sous le curseur (sans l'ouvrir)
:w %:r.cpp   sauver dans un fichier en prenant le chemin du fichier courant sans son extention et en y ajoutant .cpp
:vsp %:r.h   ouvrir en vsplit le header du même nom que le fichier courant dans le même répertoire
:e %:h       ouvrir le répertoire parent du fichier courant
:sav toto    sauver dans un fichier toto puis l'ouvrir à la place du fichier courant
<Enter>      ouvrir le fichier sous le curseur depuis l'explorateur (= quand on ouvre un dossier)
R            renommer le fichier sous le curseur depuis l'explorateur
D            effacer le fichier sous le curseur depuis l'explorateur
```

### Combiner déplacements, répétitions et commandes:

Pour bien vimer, il reste quelque détails sympas dont il faut prendre conscience:

Chaque commande peut être préfixée par un nombre ou un déplacement.  En gros,
dans vim, on peut tout combiner un peu comme dans Magicka (le jeu vidéo).  Par exemple:

```text
10ibon<Esc>    écrire 10 fois bon
gU$            mettre en majuscule jusqu'à la fin de la ligne
>k             indenter la ligne courant et la précedente
<%             si on est sur une accolade, désindenter tout le scope
1223@q         appliquer 1123 fois la macro q
{V}:!sort      trier le paragraphe courant
Qj             formater la ligne courante avec la suivante
3Yj            copier la ligne courante et les 3 suivantes
y%             copier un bloc de parenthèses (ou d'accolades...)
v%y            idem mais en utilisant le mode visual
10<C-W>+       augmente la hauteur de la fenêtre courante de 10 lignes
"ayw           copier jusqu'à la fin du mot dans le registre a
ggVG"+y        copier tout le contenu du fichier dans le presse-papier
```

### Bonus

Encore quelques commande pratiques:

```text
:r!ls      exécute une commande et insére sa sortie standard (ici ls)
<C-E>      défiler vers la bas sans changer le curseur de ligne (utile pour ne jamais utilise la souris)
<C-Y>      pareil mais vers le haut
<C-K>      affiche la page de man de la commande sous le curseur
J          joindre la ligne suivante au bout de la ligne courante
:cd :pwd   change/affiche le répertoire courant
:reg       afficher le contenue des registres
:TOhtml    génère un fichier HTML utilisant la coloration syntaxique (pratique pour mettre du code dans des slides)
ga         affiche le code ASCII du caractère sous le curseur
:retab     refaire l'indentation du fichier
```

## Un fichier de conf

Même si neovim a un comportement par défaut plus sympa que par le passé, se créer un fichier de configuration permet de se simplifier pas mal la vie. Tout est question de goût, je vous laisse étudier les lignes et prendre ce que vous voulez:

```bash
cat > ~/.config/nvim/init.vim << EOF
" Use the mouse for everything possible
set mouse=a

" Use four spaces for indentation everywhere 
set tabstop=4
set shiftwidth=4
set expandtab

" Ignore case unless upper letter typed in request
set ignorecase
set smartcase

" Use some common shortcuts
map <C-S> :update<C-M>
map <A-Left> <C-O>
map <A-Right> <C-I>

" Easy build and quickfix
map <F1> :update<C-M>:cc<C-M>
map <F2> :update<C-M>:cp<C-M>
map <F3> :update<C-M>:cn<C-M>
map <F4> :update<C-M>:make<C-M>

" Builtin gdb integration with shortcuts to use from the source window
map <F5> :packadd termdebug<C-M>:Termdebug 
let g:termdebug_wide=1
map <F9> :Break<C-M>
map <F10> :Over<C-M>
map <F11> :Step<C-M>
map <S-F11> :Finish<C-M>

" Run q macro in way compatible with keyboard repetition
map <F8> @q

" Remove trailing spaces and save
map <F12> :%s/\s\+$//ge<C-M>:update<C-M>

" Highlight word under cursor without moving it
map <Space> :cle<C-M>g*<C-O>

" Hide search highlighting
map <Esc> :nohlsearch<C-M>

" Indentation with tab (not in normal mode because this also maps <C-I>)
vmap <Tab> >>
map <S-Tab> <<

" Text formatting
map Q gq

" Back in normal mode from terminals
tmap <Esc> <C-\><C-N>

" Edit a the file under cursor even if not exists
map gf :e <cfile><C-M>

" Use backup in homedir instead of swapfile (need to make sure this directory exists)
set backupdir=~/.backup
set backup
set noswapfile

" Always copy to applications clipboard (as if always using "+ register)
set clipboard+=unnamedplus

" Switch Hex/Ascii mode using xxd external command and CTRL-H
let g:hexmodeflag=0
function! ToggleHexMode()
    if g:hexmodeflag == 1
        let g:hexmodeflag = 0
        %!xxd -r
    else
        let g:hexmodeflag = 1
        %!xxd
    endif
endfunction
map <C-H> :call ToggleHexMode()<C-M>
EOF
```

## Pour aller plus loin

Voilà, pour un tuto rapide mais qui vous emmène déjà assez loin.  Je ne
vous ai pas parlé des plugins : il en existe des tas, en particulier pour le
dev.

Il reste aussi pas mal de fonctionalités comme les onglets, la gestion des
encodings, les scripts, les enregistrements de session, l'exécution d'une
commande sur tous les buffers...  mais vim est tellement vaste qu'on peut
passer des semaines à apprendre sans en faire le tour.

Par contre, quand on commence à être à l'aise, c'est cool ;)

Sam
