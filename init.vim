" Options which are needed with old vim (not for neovim)
set hls
set incsearch
set ruler
set laststatus=2

" Use the mouse for everything possible
set mouse=a

" Use four spaces for indentation everywhere 
set tabstop=4
set shiftwidth=4
set expandtab

" In case this is not neovim
set autoindent
set smartindent
set smarttab

" Ignore case unless upper letter typed in request
set ignorecase
set smartcase

" Use some common shortcuts
map <C-S> :update<C-M>
map <A-Left> <C-O>
map <A-Right> <C-I>

set nofixendofline

" Easy build and quickfix
map <F1> :update<C-M>:cc<C-M>
map <F2> :update<C-M>:cp<C-M>
map <F3> :update<C-M>:cn<C-M>
map <F4> :update<C-M>:make<Up><C-M>

" Builtin gdb integration with shortcuts to use from the source window
map <F5> :packadd termdebug<C-M>:Termdebug 
let g:termdebug_wide=1
map <F9> :Break<C-M>
map <F10> :Over<C-M>
map <F11> :Step<C-M>
map <S-F11> :Finish<C-M>

" Run q macro in way compatible with keyboard repetition
map <F8> @q

" Remove trailing spaces and save
map <F12> :%s/\s\+$//ge<C-M>:update<C-M>

" Highlight word under cursor without moving it
map <Space> :cle<C-M>g*<C-O>

" Hide search highlighting
map <Esc> :nohlsearch<C-M>

" Indentation with tab (not in normal mode because this also maps <C-I>)
vmap <Tab> >>
map <S-Tab> <<

" Text formatting
map Q gq

" Back in normal mode from terminals
tmap <Esc> <C-\><C-N>

" Clear search with escape in normal mode
nmap <Esc> :noh<C-M>

" Use backup in homedir instead of swapfile (need to make sure this directory exists)
set backupdir=~/.backup
set backup
set noswapfile

" Always copy to applications clipboard
set clipboard+=unnamedplus

" Switch Hex/Ascii mode using xxd external command
let g:hexmodeflag=0
function! ToggleHexMode()
    if g:hexmodeflag == 1
        let g:hexmodeflag = 0
        %!xxd -r
    else
        let g:hexmodeflag = 1
        %!xxd
    endif
endfunction
map <C-H> :call ToggleHexMode()<C-M>

syntax on

set nowrap
