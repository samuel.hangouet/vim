" Vim syntax file
" Language:     dpil

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" A bunch of useful dpil keywords
syn keyword dpilKeyword    entrypoint sequential prototype intrinsic gridfunction function wildcard continue parallel strictly unsigned integer in
syn keyword dpilKeyword    alive do struct signed return break while loop else real for if goto complex union type switch case

syn keyword dpilIntent     in out inout unused bsizex bsizey sizex sizey cond gdim

syn keyword dpilMemSpace   host cudaglob cudalocal cudashared cudaconst cudashared_reduction openclglob openclconst openclimage

syn keyword dpilType       voidtype u8 u16 u32 u64 s8 s16 s32 s64 float double any complex_float complex_double longdouble complex_longdouble

"integer number, or floating point number without a dot and with "f".
syn case ignore
syn match       dpilIdent         display "`[A-Za-z_0-9]\+\|[A-Za-z_][\`A-Za-z0-9_]*"
syn match       dpilNumbers       display transparent "\<\d\|\.\d" contains=dpilNumber,dpilFloat,dpilOctalError,dpilOctal
" Same, but without octal error (for comments)
syn match       dpilNumbersCom    display contained transparent "\<\d\|\.\d" contains=dpilNumber,dpilFloat,dpilOctal
syn match       dpilNumber        display contained "\d\+\(u\=l\{0,2}\|ll\=u\)\>"
"hex number
syn match       dpilNumber        display contained "0x\x\+\(u\=l\{0,2}\|ll\=u\)\>"
" Flag the first zero of an octal number as something special
syn match       dpilOctal         display contained "0\o\+\(u\=l\{0,2}\|ll\=u\)\>" contains=dpilOctalZero
syn match       dpilOctalZero     display contained "\<0"
syn match       dpilFloat         display contained "\d\+f"
"floating point number, with dot, optional exponent
syn match       dpilFloat         display contained "\d\+\.\d*\(e[-+]\=\d\+\)\=[fl]\="
"floating point number, starting with a dot, optional exponent
syn match       dpilFloat         display contained "\.\d\+\(e[-+]\=\d\+\)\=[fl]\=\>"
"floating point number, without dot, with exponent
syn match       dpilFloat         display contained "\d\+e[-+]\=\d\+[fl]\=\>"
"hexadecimal floating point number, optional leading digits, with dot, with exponent
syn match       dpilFloat         display contained "0x\x*\.\x\+p[-+]\=\d\+[fl]\=\>"
"hexadecimal floating point number, with leading digits, optional dot, with exponent
syn match       dpilFloat         display contained "0x\x\+\.\=p[-+]\=\d\+[fl]\=\>"

syn match       dpilIntrinsic     display "\~ *\(`[A-Za-z_0-9]\+\|[A-Za-z_][\`A-Za-z0-9_]*\) *()"
" flag an octal number with wrong digits
syn match       dpilOctalError    display contained "0\o*[89]\d*"
syn case match

syn region      dpilString        start=+L\="+ skip=+\\\\\|\\"\|\\$+ excludenl end=+"+ end='$'

syn region      dpilComment       start="//" skip="\\$" end="$" keepend
syn region      dpilComment       start="/\*" end="\*/" contains=dpilComment

syn match       dpilPragma        display "^\s*#.*$"

hi def link dpilKeyword Keyword
hi def link dpilType Type
hi def link dpilNumbers Number
hi def link dpilString String
hi def link dpilComment Comment
hi def link dpilIntrinsic Label
hi def link dpilPragma PreProc
hi def link dpilIntent Label
hi def link dpilMemSpace Type
hi def link dpilIdent Normal

let b:current_syntax = "dpil"

