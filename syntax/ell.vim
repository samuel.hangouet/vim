" Vim syntax file
" Language: ell

" Quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

syn match ellEnter      display "^ *{ "
syn match ellMatch      display "^ *}="
syn match ellMismatch   display "^ *}x"
syn match ellSemAction  display "^ *| "
syn match ellBuffer     display +"[^"]*"$+

hi def link ellEnter PreProc
hi def link ellMisMatch PreProc
hi def link ellMatch Keyword
hi def link ellSemAction PreProc
hi def link ellBuffer Comment

let b:current_syntax = "ell"
